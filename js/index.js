var btn = document.querySelector('div');
var txt = document.querySelector('p');
var fun = true;

TweenMax.set(btn, {x:0, y:0, xPercent:-50, yPercent:-50})



window.addEventListener('mousemove', trackMouse);

btn.addEventListener('mouseenter', onMouseEnter);

btn.addEventListener('mouseleave', onMouseLeave);

btn.addEventListener('mousedown', onMouseDown);

btn.addEventListener('mouseup', onMouseUp);

btn.addEventListener('touchend', onTouch);


function onTouch() {
 
 // thisis acomm
 
 btn.removeEventListener('mousemove', trackMouse);
 btn.removeEventListener('mouseenter', onMouseEnter);
 btn.removeEventListener('mouseleave', onMouseLeave);
 btn.removeEventListener('mouseup', onMouseUp);
 
 txt.innerHTML = "Не тыкать пальцами, используйте мышку."
 
 TweenMax.delayedCall(3, function() {
  txt.innerHTML = "Сделать хорошо";
  
  btn.addEventListener('mousemove', trackMouse);
  btn.addEventListener('mouseenter', onMouseEnter);
  btn.addEventListener('mouseleave', onMouseLeave);
  btn.addEventListener('mouseup', onMouseUp);
 });
}

function onMouseUp() {
 
 btn.removeEventListener('mouseup', onMouseUp);
 
 btn.style.cursor = 'auto';
 
 txt.innerHTML = "Черт..."
 
 TweenMax.to(btn, 0.3, {scale:1, ease:Back.easeOut.config(5)});
 
 TweenMax.delayedCall( 2, function() {
  
  TweenMax.to(btn, 3, {x:0, y:0, ease:Power1.easeInOut});
  txt.innerHTML = "Хватит мечтать, иди работай.";
  
  btn.addEventListener('mouseenter', onMouseEnter);
  btn.addEventListener('mouseleave', onMouseLeave);
  
 })
}


function onMouseDown() {
 if( isTouchDevice() ) return;
 
 txt.innerHTML = "ай!"
 
 btn.removeEventListener('mouseenter', onMouseEnter);
 btn.removeEventListener('mouseleave', onMouseLeave);
 btn.removeEventListener('mousedown', onMouseDown);
 
 
 fun = false;
 
 TweenMax.killTweensOf(btn);
 TweenMax.to(btn, 0.8, {scale:0.9, ease:Elastic.easeOut.config(1)});
 window.removeEventListener('mousemove', trackMouse);
 
}


function onMouseEnter() {
 if( fun ) {
  txt.innerHTML = "Нееееееееет!"
 } else {
  txt.innerHTML = "Кнопка больше не работает:("
 }
}


function onMouseLeave() {
 if( fun ) {
  txt.innerHTML = "Сделать хорошо."
 } else {
  txt.innerHTML = "Хватит мечтать, иди работай!!!";
 }
}


function trackMouse(e) {
 
 if( isTouchDevice() ) return;

 var mouseX = event.clientX;     // Get the horizontal coordinate
 var mouseY = event.clientY;     // Get the vertical coordinate
 
 var btnX = btn.getBoundingClientRect().left + ( (btn.getBoundingClientRect().right - btn.getBoundingClientRect().left) / 2 );
 var btnY = btn.getBoundingClientRect().top + ( (btn.getBoundingClientRect().bottom - btn.getBoundingClientRect().top) / 2 );
 
 var distX = btnX - mouseX;
 var distY = btnY - mouseY;
 
 // The closer the mouse is, the more the button moves
 var toMoveX =  ( 1 / (distX) * 50000 );
 var toMoveY =  (1 / (distY) * 50000 );
 
 var midX = window.innerWidth * 0.5;
 var midY = window.innerHeight * 0.5;
 
 var dur = Math.abs( ( toMoveX + toMoveY ) * 0.01 );
 
 moveBox(dur, toMoveX, toMoveY)
 
 // console.log("MousX:", btnX, " midX:", "dur", dur);
 
 oldBtnX = btnX;
 oldBtnY = btnY;
 
 
}

function moveBox(dur, toMoveX, toMoveY) {
  
 TweenMax.to(btn, dur, {x:toMoveX, y:toMoveY, ease:Power4.easeOut})

}


function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}